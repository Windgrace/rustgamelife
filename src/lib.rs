mod utils;

use std::fmt;
use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Universe {
    width: usize,
    length: usize,
    cells: Vec<bool>,
}

impl Universe {
    fn next_state(&self, location: usize) -> bool {
        let mut neighbours = 0;
        let width = self.width;
        let length = self.length;
        for x_coord in &[width - 1, 0, 1] {
            for y_coord in &[length - 1, 0, 1] {
                if (*x_coord != 0 || *y_coord != 0)
                    && self.cells
                        [self.get_location((location + x_coord) % width, (location / width + y_coord) % length)]
                {
                    neighbours += 1;
                }
            }
        }

        if neighbours != 0 {
            println!("{}", neighbours);
        }
        match neighbours {
            2 => self.cells[location],
            3 => true,
            _ => false,
        }    
    }
}

impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            self.cells
                .iter()
                .map(|x| match x {
                    true => "■",
                    false => "□",
                })
                .enumerate()
                .map(|(i, x)| if i % self.width == 0 {
                    String::from("\n") + x
                } else {
                    String::from(x)
                })
                .collect::<String>()
        )
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn new(width: usize, length: usize) -> Universe {
        let mut cells = Vec::new();
        for i in 0..(width * length) {
            cells.push(i > 69 && i < 73);
        }
        Universe {
            width,
            length,
            cells,
        }
    }

//    pub fn render(&self) -> String {
//        self.to_string()
//    }
    
    pub fn get_location(&self, x_coord: usize, y_coord: usize) -> usize {
        x_coord + (y_coord * self.width)
    }

    pub fn return_cells(&self) -> *const bool {
        self.cells.as_ptr()
    }
    
//    pub fn set_cells(mut self, cell_ptr: *const bool) {
//        unsafe {    
//            self.cells = std::slice::from_raw_parts(cell_ptr, self.width * self.length).to_vec();
//        }
//    }

    pub fn toggle_nth_cell(mut self, nth: usize) -> Self {
        self.cells[nth] = !self.cells[nth];
    self
    }

    pub fn tick(mut self) -> Self {
        let new_state = self
            .cells
            .iter()
            .enumerate()
            .map(|(i, _x)| self.next_state(i));
        self.cells = new_state.collect();
        self
    }
}

#[cfg(test)]
mod tests {
    use crate::Universe as uni;
    #[test]
    fn tick_flip() {
        let mut test_vec = Vec::new();
        for i in 0..(width * length) {
            test_vec.push(i == 7 || i == 71 || i == 135);
        }
        assert_eq!(uni::new(64, 64).tick().render(), uni { width: 64, length: 64, cells: test_vec }.render());
    }
    fn neighbour_test() {
        println!("{}", uni::new(64, 64).get_location(7, 1));
        assert_eq!(uni::new(64, 64).tick().next_state(71), true);
    }
}
