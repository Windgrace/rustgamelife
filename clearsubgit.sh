#!/usr/bin/env bash
set -ex
npm init wasm-app www
mv www/.gitignore www/.gitignorewasm-app
mv www/.git www/.gitwasm-app
git fetch https://gitlab.com/Windgrace/rustgamelife.git
git reset --hard FETCH_HEAD
wasm-pack build
cd www/
npm install

