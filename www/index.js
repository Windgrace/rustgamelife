import * as wasm from "wasm-game-of-life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg.wasm";
import { Universe } from "wasm-game-of-life";

let width = 128;
let length = 128;
let lastTime = null;
let paused = false;
let prevUniverse = null;

let board = document.getElementById("board");
let life = document.getElementById("wasm-display");
let universe;
let lifeCanvas;
//let buf = new Uint8Array(memory.buffer);
//console.log(buf.slice(universe.return_cells(), universe.return_cells() + (64 * 64)));

function initUniverse() {
    document.getElementById("width").textContent = width;
    document.getElementById("height").textContent = length;
    newBoardSize();
    prevUniverse = null;
    universe = Universe.new(width, length);
    life.height = (((length) * 11) + 1);
    life.width = (((width) * 11) + 1);
    board.setAttribute("style", "width:" + life.width + "px; height:" + life.height + "px;");
    console.log(width, length, life.width, life.height);
    lifeCanvas = life.getContext("2d");
    lifeCanvas.fillStyle = 'black';
    lifeCanvas.fillRect(0, 0, life.width, life.height);
}

function cells() {
    return new Uint8Array(memory.buffer).slice(universe.return_cells(), universe.return_cells() + (width * length));
}

function paintUniverse(cells) {
    cells.forEach((cell, location) => {
            if (prevUniverse == null || cell != prevUniverse[location]) {
            lifeCanvas.fillStyle = cell == 0 ? "white" : "grey";
            lifeCanvas.fillRect((parseInt((location % width), 10) * 11) + 1, (parseInt((location / width), 10) * 11) + 1, 10, 10);
        }
    });
    prevUniverse = cells;
}

function tick(timestamp) {
    if (!lastTime) lastTime = timestamp;
    if ((lastTime + (1000/speed.textContent) < timestamp) && paused === false) {
        universe = universe.tick();
        paintUniverse(cells());
        lastTime = timestamp;
    }
    window.requestAnimationFrame(tick);
}

function clickButton(buttonId, funct) {
    document.getElementById(buttonId).addEventListener("click", funct);
}

initUniverse();
let speed = document.getElementById("speed");
function newBoardSize() {
    life.removeEventListener("click", whichCell);
    life.addEventListener("click", whichCell);
}

function whichCell(evt) {
    let lifeBounds = life.getBoundingClientRect();
    console.log(evt.screenX, evt.clientX, evt.pageX);
    let targetX = parseInt((evt.clientX - lifeBounds.x + 10) / 11 - 1, 10); 
    let targetY = parseInt((evt.clientY - lifeBounds.y + 10) / 11 - 1, 10);
    universe = universe.toggle_nth_cell(universe.get_location(targetX, targetY));
    console.log("cell toggled at:", targetX, targetY, universe.get_location(targetX, targetY));
    paintUniverse(cells());
}

clickButton("speed+", event => {speed.textContent *= 2;});
clickButton("pause-button", event => { paused = !paused;});
clickButton("speed-", event => {speed.textContent /= 2;});
clickButton("randuni", event => {
    cells().forEach((x, i) => {
        if (Math.random() >= 0.5) {
            universe = universe.toggle_nth_cell(universe.get_location(i))
        };
    });
    paintUniverse(cells());
});
clickButton("blank", event => {
    cells().forEach((x, i) => {
        if (x) {
            universe = universe.toggle_nth_cell(universe.get_location(i));
        }
    });
    paintUniverse(cells());
});
clickButton("width+", event => {
    width += 1;
    initUniverse();
});
clickButton("width-", event => {
    width -= 1;
    initUniverse();
});
clickButton("length+", event => {
    length += 1;
    initUniverse();
});
clickButton("length-", event => {
    length -= 1;
    initUniverse();
});
window.requestAnimationFrame(tick);


